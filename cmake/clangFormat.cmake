# Find all source files
set(CLANG_FORMAT_CXX_FILE_EXTENSIONS ${CLANG_FORMAT_CXX_FILE_EXTENSIONS} *.cpp *.h *.cxx *.hxx *.hpp *.cc)
file(GLOB_RECURSE ALL_SOURCE_FILES ${CLANG_FORMAT_CXX_FILE_EXTENSIONS})

find_program(CLANG_FORMAT_BIN clang-format-9)

if (NOT CLANG_FORMAT_BIN)
    message(WARNING "clang-format-9 not found!")
else ()
    add_custom_target(clang-format
            COMMENT "Use clang-format to fix all files"
            COMMAND ${CLANG_FORMAT_BIN}
            -style=file
            -i
            ${ALL_SOURCE_FILES}
            )

    add_custom_target(clang-format-check
            COMMENT "Check all files with clang-format"
            COMMAND ! ${CLANG_FORMAT_BIN}
            -style=file
            -output-replacements-xml
            ${ALL_SOURCE_FILES}
            | grep -q "replacement offset"
            )
endif ()
