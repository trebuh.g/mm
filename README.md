# MM 
MicroMouse algorithms and data representations.

Authors: Hubert Grzegorczyk (trebuh.g@gmail.com)

## Building project
### Build
Project is based on CMake and can be build using default workflow.
```
cd <repository dir>
mkdir build
cd build
cmake ..
```
To run tests execute:
```
cmake --build <path to build folder> --target tests
```
or go to build catalog:
```
cd build
make tests
```

## Checks
### clang-format
To check if code is formatted correctly run on any build:
```
make clang-format-check
```
To format code run:
```
make clang-format
```

