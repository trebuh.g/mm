//
// Created by trebuh on 17.04.2020.
//

#include <MazeMapPrinter.h>
#include <MazeMapReader.h>

#include <iostream>

#include "gtest/gtest.h"

using namespace MM;

TEST(MazeMapPrinter, printEmptyMap) { std::cout << MazeMap<5, 4>{} << std::endl; }

TEST(MazeMapPrinter, printNotEmptyMap) {
  MazeMap<8, 8> map{};
  map.addWall({0, 0}, MazeGlobalDirection::East);
  map.addWall({7, 7}, MazeGlobalDirection::West);
  map.addWall({5, 5}, MazeGlobalDirection::West);
  map.addWall({5, 5}, MazeGlobalDirection::North);
  map.addWall({5, 5}, MazeGlobalDirection::East);
  map.addWall({5, 5}, MazeGlobalDirection::South);
  std::cout << map << std::endl;
}

TEST(MazeMapPrinter, readPrintReadShouldGiveSameResult) {
  std::string map_string{
      "+--+--+--+--+--+--+--+--+\n"
      "|                    |  |\n"
      "+  +  +  +  +  +  +  +  +\n"
      "|                       |\n"
      "+  +  +  +  +  +--+  +  +\n"
      "|              |  |     |\n"
      "+  +  +  +--+  +--+  +  +\n"
      "|              |        |\n"
      "+  +  +  +--+  +--+  +  +\n"
      "|  |                    |\n"
      "+--+  +  +  +  +  +  +  +\n"
      "|        |  |     |     |\n"
      "+  +--+  +--+  +  +  +  +\n"
      "|  |                    |\n"
      "+  +--+  +  +  +  +  +  +\n"
      "|  |  |                 |\n"
      "+--+--+--+--+--+--+--+--+"};

  MM::MazeMap<8, 8> map{};
  std::stringstream map_stream{map_string};
  map_stream >> map;
  std::stringstream map_stream_out;
  map_stream_out << map;
  std::cout << map << std::endl;
  EXPECT_EQ(map_string, map_stream_out.str());
}
TEST(MazeMapPrinter, printWithPath) {
  MazeMap<5, 4> map;
  map.addWall({0,0}, MazeGlobalDirection::East);
  map.addWall({1,0}, MazeGlobalDirection::North);
  map.addWall({1,1}, MazeGlobalDirection::West);
  printMapWithPath(std::cout, map, Path({{0, 0}, {1, 0}, {1, 1}})) << std::endl;
}