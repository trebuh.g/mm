//
// Created by trebuh on 17.04.2020.
//

#include <MazeMap.h>
#include <MazeMapPrinter.h>
#include <MazeMapReader.h>
#include <MazePositionPrinter.h>
#include <MazeSolver.h>
#include <MazeSolverErrorPrinter.h>

#include <string>

#include "gtest/gtest.h"

namespace {

bool besideAndNotDiagonally(MM::MazePosition l, MM::MazePosition r) {
  if (std::abs(l.x - r.x) == 1 && std::abs(l.y - r.y) == 0) {
    return true;
  } else if (std::abs(l.x - r.x) == 0 && std::abs(l.y - r.y) == 1) {
    return true;
  }
  return false;
}

MM::MazeGlobalDirection getDirectionBetweenNeighbourNodes(MM::MazePosition from, MM::MazePosition to) {
  if (to.x - from.x == 1 && to.y - from.y == 0) {
    return MM::MazeGlobalDirection::East;
  } else if (to.x - from.x == -1 && to.y - from.y == 0) {
    return MM::MazeGlobalDirection::West;
  } else if (to.x - from.x == 0 && to.y - from.y == 1) {
    return MM::MazeGlobalDirection::North;
  }
  return MM::MazeGlobalDirection::South;
}

template <uint16_t SizeX, uint16_t SizeY, uint32_t PathSize>
void validatePath(const MM::Path<PathSize>& path, const MM::MazeMap<SizeX, SizeY>& map, MM::MazePosition from,
                  MM::MazePosition to) {
  for (uint32_t i = 0; i < path.size(); ++i) {
    ASSERT_TRUE(path[i].x >= 0 && path[i].x < SizeX) << "Path node x coordinate must be inside maze! Failed for node "
                                                        "number: "
                                                     << i << " which was " << path[i];
    ASSERT_TRUE(path[i].y >= 0 && path[i].y < SizeX) << "Path node y coordinate must be inside maze! Failed for node "
                                                        "number: "
                                                     << i << " which was " << path[i];
  }
  EXPECT_TRUE(path.size() >= 2);
  EXPECT_EQ(*path.cbegin(), from);
  EXPECT_EQ(*(path.cend() - 1), to);

  // check continuity of the path
  for (uint32_t i = 0; i < path.size() - 1; ++i) {
    ASSERT_TRUE(besideAndNotDiagonally(path[i], path[i + 1]))
        << "Path has to be continuous, node " << i << " " << path[i] << " is not beside node " << i + 1 << " "
        << path[i + 1] << " or nodes are diagonal to each other or it is the same field.";
    auto direction{getDirectionBetweenNeighbourNodes(path[i], path[i + 1])};
    ASSERT_FALSE(map.checkWall(path[i], direction))
        << "Path cannot go though wall! Between nodes " << i << " " << path[i] << " and " << i + 1 << " " << path[i + 1]
        << " there is wall.";
  }
  MM::Path<PathSize> path_copy{path};
  ASSERT_TRUE(std::unique(path_copy.begin(), path_copy.end()) == path_copy.cend())
      << "Path should not pass multiple times through one field!";
}
}  // namespace

using namespace MM;

class MazeSolverTest4x4
    : public ::testing::TestWithParam<std::tuple<std::string, MazeSolverError, MazePosition, MazePosition>> {
 protected:
  MazeMap<4, 4> map;
};

TEST_P(MazeSolverTest4x4, checkIfValidPathGenerated) {
  MazePosition from = std::get<2>(GetParam());
  MazePosition to = std::get<3>(GetParam());
  std::string map_string = std::get<0>(GetParam());
  auto expected_result = std::get<1>(GetParam());

  std::stringstream map_stream(map_string);
  map_stream >> map;
  MazeSolver solver(map);
  Path<16> path;
  EXPECT_EQ(solver.solve(from, to, path), expected_result);
  if (MazeSolverError::Ok == expected_result) {
    validatePath(path, map, from, to);
    printMapWithPath(std::cout, map, path) << std::endl;
  } else if (MazeSolverError::NoPathToTarget == expected_result) {
    std::cout << map << std::endl;
  }
}

INSTANTIATE_TEST_SUITE_P(
    MazeSolverTests, MazeSolverTest4x4,
    ::testing::Values(std::make_tuple(std::string("+--+--+--+--+\n"
                                                  "|           |\n"
                                                  "+  +  +  +  +\n"
                                                  "|           |\n"
                                                  "+  +  +  +  +\n"
                                                  "|           |\n"
                                                  "+  +  +  +  +\n"
                                                  "|           |\n"
                                                  "+--+--+--+--+"),
                                      MazeSolverError::Ok, MazePosition{0, 0}, MazePosition{3, 3}),
                      std::make_tuple(std::string("+--+--+--+--+\n"
                                                  "|     |     |\n"
                                                  "+  +  +--+  +\n"
                                                  "|  |     |  |\n"
                                                  "+  +--+  +--+\n"
                                                  "|        |  |\n"
                                                  "+--+--+  +  +\n"
                                                  "|           |\n"
                                                  "+--+--+--+--+"),
                                      MazeSolverError::NoPathToTarget, MazePosition{0, 0}, MazePosition{3, 3}),
                      std::make_tuple(std::string("+--+--+--+--+\n"
                                                  "|           |\n"
                                                  "+--+  +--+--+\n"
                                                  "|           |\n"
                                                  "+  +--+--+--+\n"
                                                  "|           |\n"
                                                  "+--+--+--+  +\n"
                                                  "|           |\n"
                                                  "+--+--+--+--+"),
                                      MazeSolverError::Ok, MazePosition{2, 2}, MazePosition{2, 1}),
                      std::make_tuple(std::string("+--+--+--+--+\n"
                                                  "|     |     |\n"
                                                  "+  +  +  +  +\n"
                                                  "|  |     |  |\n"
                                                  "+  +--+  +--+\n"
                                                  "|     |     |\n"
                                                  "+  +  +--+--+\n"
                                                  "|  |        |\n"
                                                  "+--+--+--+--+"),
                                      MazeSolverError::Ok, MazePosition{3, 0}, MazePosition{2, 1})));

TEST(MazeSolverTest16x16, shouldTakeShorterPath) {
  MazePosition from{0, 0};
  MazePosition to{8, 8};

  MazeMap<16, 16> map;

  std::string map_string{
      "+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+\n"
      "|                                               |\n"
      "+  +  +  +  +  +  +  +--+--+--+--+--+--+--+--+  +\n"
      "|                    |                          |\n"
      "+--+--+--+--+--+--+  +  +--+--+--+--+--+--+--+--+\n"
      "|                    |                          |\n"
      "+  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+  +\n"
      "|                    |                          |\n"
      "+--+--+--+--+--+--+  +  +--+--+--+--+--+--+--+--+\n"
      "|                    |                          |\n"
      "+  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+  +\n"
      "|                    |                          |\n"
      "+--+--+--+--+--+--+  +  +--+--+--+--+--+--+--+--+\n"
      "|                    |                          |\n"
      "+  +--+--+--+--+--+--+--+--+  +  +  +  +  +  +  +\n"
      "|                    |     |                    |\n"
      "+--+--+--+--+--+--+  +  +  +--+  +  +  +  +  +  +\n"
      "|                    |                          |\n"
      "+  +  +  +  +  +  +--+--+--+  +  +  +  +  +  +  +\n"
      "|                 |        |  |                 |\n"
      "+  +  +  +  +  +  +  +  +  +  +  +  +  +  +  +  +\n"
      "|                 |        |  |                 |\n"
      "+  +  +  +  +  +  +  +  +  +  +  +  +  +  +  +  +\n"
      "|                 |        |  |                 |\n"
      "+  +  +  +  +  +  +  +  +  +  +  +  +  +  +  +  +\n"
      "|              |  |        |  |                 |\n"
      "+  +--+--+  +--+  +  +  +  +  +  +  +  +  +  +  +\n"
      "|     |           |        |  |                 |\n"
      "+  +--+  +--+  +  +  +  +  +  +  +  +  +  +  +  +\n"
      "|  |     |        |        |  |                 |\n"
      "+  +  +--+--+--+--+--+--+--+  +  +  +  +  +  +  +\n"
      "|  |                          |                 |\n"
      "+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+"};

  std::stringstream map_stream(map_string);
  map_stream >> map;
  MazeSolver solver(map);
  Path path;
  EXPECT_EQ(solver.solve(from, to, path), MazeSolverError::Ok);
  const Path shortest_path({{0, 0}, {0, 1}, {0, 2}, {0, 3}, {1, 3}, {2, 3}, {3, 3}, {3, 2}, {2, 2}, {2, 1},
                      {1, 1}, {1, 0}, {2, 0}, {3, 0}, {4, 0}, {5, 0}, {6, 0}, {7, 0}, {8, 0}, {9, 0},
                      {9, 1}, {9, 2}, {9, 3}, {9, 4}, {9, 5}, {9, 6}, {9, 7}, {8, 7}, {8, 8}});
  validatePath(path, map, from, to);

  printMapWithPath(std::cout, map, path) << std::endl;
}