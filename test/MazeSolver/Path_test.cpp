//
// Created by trebuh on 17.04.2020.
//

#include <MazePositionPrinter.h>
#include <Path.h>

#include <algorithm>

#include "gtest/gtest.h"

using namespace MM;

TEST(Path, createSimplePath) {
  auto nodes = {MazePosition{1, 1}, MazePosition{2, 2}, MazePosition{3, 3}};

  Path<10> path;

  for (const auto& node : nodes) {
    path.addNodeAtEnd(node);
  }
  EXPECT_EQ(path.size(), nodes.size());
  EXPECT_TRUE(std::equal(path.cbegin(), path.cend(), nodes.begin()));

  path.clear();
  EXPECT_EQ(path.size(), 0);
}
