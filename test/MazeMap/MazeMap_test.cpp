//
// Created by trebuh on 17.04.2020.
//

#include <MazeMap.h>

#include "gtest/gtest.h"

using namespace MM;

TEST(MazeMap, compiles) { MazeMap<> map; }

TEST(MazeMap, addWallAndCheckIfAddedAndDelete) {
  MazeMap<7, 4> map;
  MazePosition position{1, 2};
  MazePosition position_to_north{1, 3};
  MazePosition position_to_east{2, 2};
  map.addWall(position, MazeGlobalDirection::North);
  EXPECT_TRUE(map.checkWall(position, MazeGlobalDirection::North));
  EXPECT_TRUE(map.checkWall(position_to_north, MazeGlobalDirection::South));

  map.addWall(position, MazeGlobalDirection::East);
  EXPECT_TRUE(map.checkWall(position, MazeGlobalDirection::East));
  EXPECT_TRUE(map.checkWall(position_to_east, MazeGlobalDirection::West));

  map.deleteWall(position, MazeGlobalDirection::North);
  EXPECT_FALSE(map.checkWall(position, MazeGlobalDirection::North));
  EXPECT_FALSE(map.checkWall(position_to_north, MazeGlobalDirection::South));

  map.deleteWall(position, MazeGlobalDirection::East);
  EXPECT_FALSE(map.checkWall(position, MazeGlobalDirection::East));
  EXPECT_FALSE(map.checkWall(position_to_east, MazeGlobalDirection::West));
}

TEST(MazeMap, checkIfOuterWallIsSet) {
  const int sizeX = 4, sizeY = 6;
  MazeMap<sizeX, sizeY> map;

  for (uint16_t x = 0; x < sizeX; ++x) {
    EXPECT_TRUE(map.checkWall({x, 0}, MazeGlobalDirection::South));
    EXPECT_TRUE(map.checkWall({x, sizeY - 1}, MazeGlobalDirection::North));
  }
  for (uint16_t y = 0; y < sizeY; ++y) {
    EXPECT_TRUE(map.checkWall({0, y}, MazeGlobalDirection::West));
    EXPECT_TRUE(map.checkWall({sizeX - 1, y}, MazeGlobalDirection::East));
  }
}

TEST(MazeMap, zeroedAfterConstruction) {
  const int sizeX = 10, sizeY = 10;
  MazeMap<sizeX, sizeY> map;

  // All zeroed except outer walls
  for (uint16_t x = 0; x < sizeX; ++x) {
    for (uint16_t y = 0; y < sizeY; ++y) {
      MazePosition position{x, y};
      if (x != 0) {
        EXPECT_FALSE(map.checkWall(position, MazeGlobalDirection::West));
      }
      if (x != sizeX - 1) {
        EXPECT_FALSE(map.checkWall(position, MazeGlobalDirection::East));
      }
      if (y != 0) {
        EXPECT_FALSE(map.checkWall(position, MazeGlobalDirection::South));
      }
      if (y != sizeY - 1) {
        EXPECT_FALSE(map.checkWall(position, MazeGlobalDirection::North));
      }
    }
  }
}

TEST(MazeMap, setAndDeleteOnEdges) {
  const int sizeX = 16, sizeY = 16;
  MazeMap<sizeX, sizeY> map;
  MazePosition pos{15, 15};
  map.addWall(pos, MazeGlobalDirection::North);
  EXPECT_TRUE(map.checkWall(pos, MazeGlobalDirection::North));

  map.deleteWall(pos, MazeGlobalDirection::North);
  EXPECT_FALSE(map.checkWall(pos, MazeGlobalDirection::North));

  MazePosition pos2{0, 0};
  map.addWall(pos2, MazeGlobalDirection::South);
  EXPECT_TRUE(map.checkWall(pos2, MazeGlobalDirection::South));

  map.deleteWall(pos2, MazeGlobalDirection::South);
  EXPECT_FALSE(map.checkWall(pos2, MazeGlobalDirection::South));
}