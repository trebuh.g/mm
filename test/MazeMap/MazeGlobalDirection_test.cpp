//
// Created by trebuh on 17.04.2020.
//

#include <MazeGlobalDirection.h>

#include "gtest/gtest.h"

using namespace MM;

TEST(MazeGlobalDirection, checkRotations) {
  for (auto dir :
       {MazeGlobalDirection::North, MazeGlobalDirection::East, MazeGlobalDirection::South, MazeGlobalDirection::West}) {
    EXPECT_EQ(rotateCCW90Deg(rotateCW90Deg(dir)), dir);
    EXPECT_EQ(rotateCW90Deg(rotateCW90Deg(rotateCW90Deg(rotateCW90Deg(dir)))), dir);
    EXPECT_EQ(rotateCCW90Deg(rotateCCW90Deg(rotateCCW90Deg(rotateCCW90Deg(dir)))), dir);
  }
}

TEST(MazeGlobalDirection, checkOpposite) {
  EXPECT_EQ(opposite(MazeGlobalDirection::North), MazeGlobalDirection::South);
  EXPECT_EQ(opposite(MazeGlobalDirection::East), MazeGlobalDirection::West);
  EXPECT_EQ(opposite(MazeGlobalDirection::South), MazeGlobalDirection::North);
  EXPECT_EQ(opposite(MazeGlobalDirection::West), MazeGlobalDirection::East);
}
