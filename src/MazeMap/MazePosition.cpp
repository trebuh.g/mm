//
// Created by trebuh on 18.04.2020.
//
#include "MazePosition.h"

#include <tuple>

namespace MM {
void MazePosition::move(MazeGlobalDirection direction) noexcept {
  switch (direction) {
    case MazeGlobalDirection::North:
      ++y;
      break;
    case MazeGlobalDirection::South:
      --y;
      break;
    case MazeGlobalDirection::West:
      --x;
      break;
    case MazeGlobalDirection::East:
      ++x;
      break;
    default:
      break;
  }
}
bool MazePosition::operator==(const MazePosition& rhs) const { return std::tie(x, y) == std::tie(rhs.x, rhs.y); }
bool MazePosition::operator!=(const MazePosition& rhs) const { return !(*this == rhs); }
}  // namespace MM