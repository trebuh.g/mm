//
// Created by trebuh on 17.04.2020.
//

#ifndef MM_MAZE_MAZEMAP_H
#define MM_MAZE_MAZEMAP_H

#include <bitset>
#include <cstdint>

#include "MazeGlobalDirection.h"
#include "MazePosition.h"

namespace MM {

namespace impl {

template <uint16_t SizeX, uint16_t SizeY>
struct ByteSizeOfMazeMap {
  /**
   * 5 bits per field.
   * Bit 0 - is there north wall
   * Bit 1 - is there east wall
   * Bit 2 - is there south wall
   * Bit 3 - is there wall wall
   *
   * @return size in bytes of needed container.
   */
  constexpr static uint8_t bits_per_field = 4;
  constexpr static size_t sizeBytes() noexcept { return SizeY * SizeX * bits_per_field; };
};

}  // namespace impl

/**
 * N
 * ^ y
 * |
 * |
 * |
 * |
 * |
 * ----------------> x E
 * (0,0)
 *
 * @tparam SizeX
 * @tparam SizeY
 */
template <uint16_t SizeX = 16, uint16_t SizeY = 16>
class MazeMap {
 public:
  constexpr MazeMap() noexcept;
  void addWall(MazePosition, MazeGlobalDirection) noexcept;
  void deleteWall(MazePosition, MazeGlobalDirection) noexcept;
  constexpr bool checkWall(MazePosition, MazeGlobalDirection) const noexcept;
  constexpr bool validPosition(MazePosition) const noexcept;
 private:
  constexpr uint32_t bitPosition(MazePosition, MazeGlobalDirection) const noexcept;

 private:
  std::bitset<impl::ByteSizeOfMazeMap<SizeX, SizeY>::sizeBytes()> map_data;
};

template <uint16_t SizeX, uint16_t SizeY>
constexpr MazeMap<SizeX, SizeY>::MazeMap() noexcept {
  // set outer wall
  for (uint16_t x = 0; x < SizeX; ++x) {
    map_data.set(bitPosition({x, SizeY - 1}, MazeGlobalDirection::North));
    map_data.set(bitPosition({x, 0}, MazeGlobalDirection::South));
  }
  for (uint16_t y = 0; y < SizeY; ++y) {
    map_data.set(bitPosition({0, y}, MazeGlobalDirection::West));
    map_data.set(bitPosition({SizeX - 1, y}, MazeGlobalDirection::East));
  }
}

template <uint16_t SizeX, uint16_t SizeY>
void MazeMap<SizeX, SizeY>::addWall(MazePosition pos, MazeGlobalDirection dir) noexcept {
  map_data.set(bitPosition(pos, dir));
  pos.move(dir);
  if (validPosition(pos)) {
    map_data.set(bitPosition(pos, opposite(dir)));
  }
}

template <uint16_t SizeX, uint16_t SizeY>
void MazeMap<SizeX, SizeY>::deleteWall(MazePosition pos, MazeGlobalDirection dir) noexcept {
  map_data.reset(bitPosition(pos, dir));
  pos.move(dir);
  if (validPosition(pos)) {
    map_data.reset(bitPosition(pos, opposite(dir)));
  }
}

template <uint16_t SizeX, uint16_t SizeY>
constexpr bool MazeMap<SizeX, SizeY>::checkWall(MazePosition pos, MazeGlobalDirection dir) const noexcept {
  return map_data.test(bitPosition(pos, dir));
}

template <uint16_t SizeX, uint16_t SizeY>
constexpr uint32_t MazeMap<SizeX, SizeY>::bitPosition(MazePosition pos, MazeGlobalDirection dir) const noexcept {
  return SizeX * impl::ByteSizeOfMazeMap<SizeX, SizeY>::bits_per_field * pos.y +
         pos.x * impl::ByteSizeOfMazeMap<SizeX, SizeY>::bits_per_field + static_cast<uint32_t>(dir);
}
template <uint16_t SizeX, uint16_t SizeY>
constexpr bool MazeMap<SizeX, SizeY>::validPosition(MazePosition pos) const noexcept {
  if (pos.x < SizeX && pos.y < SizeY && pos.y >= 0 && pos.x >= 0) {
    return true;
  }
  return false;
}

}  // namespace MM

#endif  // MM_MAZE_MAZEMAP_H
