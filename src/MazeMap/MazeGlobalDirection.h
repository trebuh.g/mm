//
// Created by trebuh on 17.04.2020.
//

#ifndef MM_MAZE_MAZEGLOBALDIRECTION_H
#define MM_MAZE_MAZEGLOBALDIRECTION_H

namespace MM {
enum class MazeGlobalDirection { North = 0, East, South, West };

MazeGlobalDirection opposite(MazeGlobalDirection dir) noexcept;

MazeGlobalDirection rotateCW90Deg(MazeGlobalDirection dir) noexcept;

MazeGlobalDirection rotateCCW90Deg(MazeGlobalDirection dir) noexcept;

}  // namespace MM
#endif  // MM_MAZE_MAZEGLOBALDIRECTION_H
