//
// Created by trebuh on 18.04.2020.
//

#ifndef MM_MAZE_MAZEPOSITION_H
#define MM_MAZE_MAZEPOSITION_H

#include <cstdint>

#include "MazeGlobalDirection.h"

namespace MM {
class MazePosition {
 public:
  int32_t x;
  int32_t y;
  /**
   * Moves position in maze in given global direction.
   * No bound check is performed.
   */
  void move(MazeGlobalDirection) noexcept;
  bool operator==(const MazePosition& rhs) const;
  bool operator!=(const MazePosition& rhs) const;
};

}  // namespace MM

#endif  // MM_MAZE_MAZEPOSITION_H
