//
// Created by trebuh on 18.04.2020.
//

#include "MazeGlobalDirection.h"

#include <cstdint>

namespace MM {

namespace {
constexpr static int8_t directions_count = 4;
inline MazeGlobalDirection rotate(int8_t count, MazeGlobalDirection dir) {
  int8_t dir_number = static_cast<int32_t>(dir);
  dir_number += count;
  if (dir_number >= directions_count) {
    dir_number -= directions_count;
  }
  return static_cast<MazeGlobalDirection>(dir_number);
}
}  // namespace

MazeGlobalDirection opposite(MazeGlobalDirection dir) noexcept { return rotate(2, dir); }

MazeGlobalDirection rotateCW90Deg(MazeGlobalDirection dir) noexcept { return rotate(1, dir); }

MazeGlobalDirection rotateCCW90Deg(MazeGlobalDirection dir) noexcept { return rotate(3, dir); }
}  // namespace MM