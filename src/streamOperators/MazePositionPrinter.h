//
// Created by trebuh on 19.04.2020.
//

#ifndef MM_MAZEPOSITIONPRINTER_H
#define MM_MAZEPOSITIONPRINTER_H

#include <MazePosition.h>
#include <ostream>

namespace MM {

std::ostream& operator<<(std::ostream& os, const MM::MazePosition& pos) {
  os << '{' << pos.x << ", " << pos.y << '}';
  return os;
}

}  // namespace MM

#endif  // MM_MAZEPOSITIONPRINTER_H
