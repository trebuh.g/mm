//
// Created by trebuh on 18.04.2020.
//

#ifndef MM_MAZEMAPPRINTER_H
#define MM_MAZEMAPPRINTER_H

#include <MazeMap.h>
#include <Path.h>

#include <algorithm>
#include <cstdint>
#include <functional>
#include <ostream>
#include <string>

namespace MM {
namespace impl {
template <uint16_t SizeX>
void doForEachX(std::function<void(uint16_t)> fun) {
  for (uint16_t x = 0; x < SizeX; ++x) {
    fun(x);
  }
}

template <uint16_t SizeX, uint16_t SizeY>
void printWithActionForFieldContent(std::ostream& os, const MazeMap<SizeX, SizeY>& map,
                                    std::function<char(MM::MazePosition)> fun) {
  os << '+';
  impl::doForEachX<SizeX>([&](auto) { os << "--+"; });
  for (int32_t y = SizeY - 1; y >= 0; --y) {
    os << std::endl;
    os << '|';
    impl::doForEachX<SizeX>([&](auto x) {
      os << fun({x, y}) << (map.checkWall({x, y}, MazeGlobalDirection::East) ? " |" : "  ");
    });
    os << std::endl << '+';
    impl::doForEachX<SizeX>([&](auto x) {
      os << (map.checkWall({x, y}, MazeGlobalDirection::South) ? "--" : "  ") << '+';
    });
  }
}

}  // namespace impl

template <uint16_t SizeX, uint16_t SizeY>
std::ostream& operator<<(std::ostream& os, const MazeMap<SizeX, SizeY>& map) {
  // print top of maze
  impl::printWithActionForFieldContent(os, map, [](auto) { return ' '; });
  return os;
}

template <uint16_t SizeX, uint16_t SizeY, uint32_t PathSize>
std::ostream& printMapWithPath(std::ostream& os, const MazeMap<SizeX, SizeY>& map, const Path<PathSize>& path) {
  // print top of maze
  impl::printWithActionForFieldContent(os, map, [&](auto pos) {
    if (std::find(path.cbegin(), path.cend(), pos) != path.cend()) {
      return 'x';
    }
    return ' ';
  });
  return os;
}
}  // namespace MM

#endif  // MM_MAZEMAPPRINTER_H
