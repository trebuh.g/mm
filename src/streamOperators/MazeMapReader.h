//
// Created by trebuh on 18.04.2020.
//

#ifndef MM_MAZEMAPREADER_H
#define MM_MAZEMAPREADER_H

#include <MazeMap.h>

#include <exception>
#include <functional>
#include <istream>

namespace MM {
template <uint16_t SizeX, uint16_t SizeY>
std::istream &operator>>(std::istream &in, MM::MazeMap<SizeX, SizeY> &map) {
  // read top
  std::string line;
  std::getline(in, line);
  for (int32_t x = 0; x < SizeX; ++x) {
    if (line[x * 3] != '+') {
      throw std::runtime_error("Wrong format: expected '+'");
    }
    if (line[x * 3 + 1] == '-' && line[x * 3 + 2] == '-') {
      map.addWall({x, SizeY - 1}, MM::MazeGlobalDirection::North);
    }
  }
  for (int32_t y = SizeY - 1; y >= 0; --y) {
    std::getline(in, line);
    for (int32_t x = 0; x < SizeX; ++x) {
      if (line[x * 3] == '|') {
        map.addWall({x, y}, MM::MazeGlobalDirection::West);
      } else if (line[x * 3] == ' ') {
        map.deleteWall({x, y}, MM::MazeGlobalDirection::West);
      } else {
        throw std::runtime_error("Wrong format: expected '|' or ' '");
      }
    }
    if (line[SizeX * 3 - 1] == '|') {
      map.addWall({SizeX - 1, y}, MM::MazeGlobalDirection::East);
    }
    std::getline(in, line);
    for (int32_t x = 0; x < SizeX; ++x) {
      if (line[x * 3] != '+') {
        throw std::runtime_error("Wrong format: expected '+'");
      }
      if (line[x * 3 + 1] == '-' && line[x * 3 + 2] == '-') {
        map.addWall({x, y}, MM::MazeGlobalDirection::South);
      } else if (line[x * 3 + 1] == ' ' && line[x * 3 + 2] == ' ') {
        map.deleteWall({x, y}, MM::MazeGlobalDirection::South);
      } else {
        throw std::runtime_error("Wrong format: expected '--' or '  '");
      }
    }
  }

  return in;
}
}  // namespace MM

#endif  // MM_MAZEMAPREADER_H
