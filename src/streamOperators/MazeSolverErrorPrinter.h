//
// Created by trebuh on 19.04.2020.
//

#ifndef MM_MAZESOLVERERRORPRINTER_H
#define MM_MAZESOLVERERRORPRINTER_H

#include <MazeSolverError.h>

namespace MM {
std::ostream& operator<<(std::ostream& os, const MazeSolverError& error) {
  switch (error) {
    case MazeSolverError::Ok:
      os << "MazeSolverError::Ok";
      break;
    case MazeSolverError::NoPathToTarget:
      os << "MazeSolverError::NoPathToTarget";
      break;
  }
  return os;
}
}

#endif  // MM_MAZESOLVERERRORPRINTER_H
