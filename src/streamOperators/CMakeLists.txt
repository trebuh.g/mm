add_library(MM_streamOperators INTERFACE)

target_sources(MM_streamOperators INTERFACE
        MazeMapPrinter.h
        MazeMapReader.h
        MazeSolverErrorPrinter.h
        MazePositionPrinter.h
        )

add_library(MM::streamOperators ALIAS MM_streamOperators)

target_link_libraries(MM_streamOperators INTERFACE
        MM::MazeMap
        MM::MazeSolver)

target_include_directories(MM_streamOperators INTERFACE ./)

target_compile_features(MM_streamOperators INTERFACE cxx_std_11)
