//
// Created by trebuh on 19.04.2020.
//

#ifndef MM_MAZESOLVERERROR_H
#define MM_MAZESOLVERERROR_H
namespace MM {
enum class MazeSolverError { Ok = 0, NoPathToTarget };
}
#endif  // MM_MAZESOLVERERROR_H
