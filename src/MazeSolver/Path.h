//
// Created by trebuh on 19.04.2020.
//

#ifndef MM_PATH_H
#define MM_PATH_H

#include <MazePosition.h>

#include <array>
#include <cstdint>

namespace MM {

template <uint32_t MaxLength = 16 * 16>
class Path {
  using ArrayType = std::array<MazePosition, MaxLength>;

 public:
  Path();
  Path(std::initializer_list<MazePosition> list);

  // TODO bound/size checking?
  void addNodeAtEnd(MazePosition);
  uint32_t size() const ;
  void clear();

  MazePosition& operator[](uint32_t pos);
  const MazePosition& operator[](uint32_t pos) const;
  typename ArrayType::iterator begin() noexcept;
  typename ArrayType::const_iterator begin() const noexcept;
  typename ArrayType::const_iterator cbegin() const noexcept;
  typename ArrayType::iterator end() noexcept;
  typename ArrayType::const_iterator end() const noexcept;
  typename ArrayType::const_iterator cend() const noexcept;

 private:
  uint32_t path_size = 0;
  ArrayType path{};
};

template <uint32_t MaxLength>
Path<MaxLength>::Path() {}

template <uint32_t MaxLength>
Path<MaxLength>::Path(std::initializer_list<MazePosition> list) {
  for (const auto& node: list)
  {
    addNodeAtEnd(node);
  }
}

template <uint32_t MaxLength>
void Path<MaxLength>::addNodeAtEnd(MazePosition position) {
  path[path_size++] = position;
}
template <uint32_t MaxLength>
uint32_t Path<MaxLength>::size() const{
  return path_size;
}
template <uint32_t MaxLength>
void Path<MaxLength>::clear() {
  path_size = 0;
}
template <uint32_t MaxLength>
MazePosition& Path<MaxLength>::operator[](uint32_t pos) {
  return path[pos];
}

template <uint32_t MaxLength>
const MazePosition& Path<MaxLength>::operator[](uint32_t pos) const {
  return path[pos];
}
template <uint32_t MaxLength>
typename Path<MaxLength>::ArrayType::iterator Path<MaxLength>::begin() noexcept {
  return path.begin();
}
template <uint32_t MaxLength>
typename Path<MaxLength>::ArrayType::const_iterator Path<MaxLength>::begin() const noexcept {
  return path.begin();
}
template <uint32_t MaxLength>
typename Path<MaxLength>::ArrayType::const_iterator Path<MaxLength>::cbegin() const noexcept {
  return path.cbegin();
}
template <uint32_t MaxLength>
typename Path<MaxLength>::ArrayType::iterator Path<MaxLength>::end() noexcept {
  return path.begin() + path_size;
}
template <uint32_t MaxLength>
typename Path<MaxLength>::ArrayType::const_iterator Path<MaxLength>::end() const noexcept {
  return path.begin() + path_size;
}
template <uint32_t MaxLength>
typename Path<MaxLength>::ArrayType::const_iterator Path<MaxLength>::cend() const noexcept {
  return path.cbegin() + path_size;
}


}  // namespace MM

#endif  // MM_PATH_H
