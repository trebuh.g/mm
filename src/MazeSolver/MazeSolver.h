//
// Created by trebuh on 19.04.2020.
//

#ifndef MM_MAZESOLVER_H
#define MM_MAZESOLVER_H

#include <MazeMap.h>
#include <MazePosition.h>
#include <MazeSolverError.h>

#include <cstdint>

#include "Path.h"

namespace MM {

template <uint16_t SizeX, uint16_t SizeY, typename MazeMapType = MazeMap<SizeX, SizeY>>
class MazeSolver {
 public:
  MazeSolver(const MazeMapType& map);
  template <uint32_t PathSize>
  [[nodiscard]] MazeSolverError solve(MazePosition from, MazePosition to, Path<PathSize>& path);

 private:
  void clearFloodfillArray() noexcept;
  void doFloodfill(MazePosition from) noexcept;
  void expandFloodFillFieldIfEqualTo(MazePosition field, uint16_t value, bool& expanded) noexcept;
  template <uint32_t PathSize>
  MazeSolverError createPathFromFloodfill(MazePosition from, MazePosition to, Path<PathSize>& path);

 private:
  std::array<std::array<uint16_t, SizeY>, SizeX> floodfill_values;  // TODO size of floodfill variable? it may be
                                                                    // uint8_t
  const MazeMapType& map;
};

template <uint16_t SizeX, uint16_t SizeY, typename MazeMapType>
MazeSolver<SizeX, SizeY, MazeMapType>::MazeSolver(const MazeMapType& map) : map(map) {}

template <uint16_t SizeX, uint16_t SizeY, typename MazeMapType>
template <uint32_t PathSize>
MazeSolverError MazeSolver<SizeX, SizeY, MazeMapType>::solve(MazePosition from, MazePosition to, Path<PathSize>& path) {
  static_assert(PathSize >= SizeX * SizeY, "Path MaxSize should not be less than fields count in maze");
  path.clear();
  clearFloodfillArray();
  // TODO simple algorithm, more complex can take into account that turning is worse
  doFloodfill(to);
  return createPathFromFloodfill(from, to, path);
}

template <uint16_t SizeX, uint16_t SizeY, typename MazeMapType>
void MazeSolver<SizeX, SizeY, MazeMapType>::clearFloodfillArray() noexcept {
  for (auto& column : floodfill_values) {
    for (auto& field : column) {
      field = 0;
    }
  }
}

template <uint16_t SizeX, uint16_t SizeY, typename MazeMapType>
void MazeSolver<SizeX, SizeY, MazeMapType>::doFloodfill(MazePosition from) noexcept {
  floodfill_values[from.x][from.y] = 1;
  bool end_cond = true;
  uint32_t act_number = 1;  // start from 1
  while (end_cond) {        // fill up until there is anything to expand
    end_cond = false;
    for (uint16_t y = 0; y < SizeY; ++y) {  // search for act_number and expand
      for (uint16_t x = 0; x < SizeX; ++x) {
        expandFloodFillFieldIfEqualTo({x, y}, act_number, end_cond);
      }
    }
    ++act_number;
  }
}
template <uint16_t SizeX, uint16_t SizeY, typename MazeMapType>
void MazeSolver<SizeX, SizeY, MazeMapType>::expandFloodFillFieldIfEqualTo(MazePosition field, uint16_t value,
                                                                          bool& expanded) noexcept {
  if (floodfill_values[field.x][field.y] != value) {
    return;
  }
  for (const auto direction :
       {MazeGlobalDirection::North, MazeGlobalDirection::South, MazeGlobalDirection::East, MazeGlobalDirection::West}) {
    MazePosition new_field = field;
    new_field.move(direction);
    if (!map.checkWall(field, direction) && floodfill_values[new_field.x][new_field.y] == 0) {
      floodfill_values[new_field.x][new_field.y] = value + 1;
      expanded = true;
    }
  }
}
template <uint16_t SizeX, uint16_t SizeY, typename MazeMapType>
template <uint32_t PathSize>
MazeSolverError MazeSolver<SizeX, SizeY, MazeMapType>::createPathFromFloodfill(MazePosition from, MazePosition to,
                                                                               Path<PathSize>& path) {
  if (floodfill_values[from.x][from.y] == 0) {
    return MazeSolverError::NoPathToTarget;
  }
  MazePosition position{from};
  path.addNodeAtEnd(from);
  while (position != to) {
    for (const auto direction : {MazeGlobalDirection::North, MazeGlobalDirection::South, MazeGlobalDirection::East,
                                 MazeGlobalDirection::West}) {
      MazePosition new_position = position;
      new_position.move(direction);
      if (!map.validPosition(new_position)) {
        continue;
      }
      if (floodfill_values[new_position.x][new_position.y] == floodfill_values[position.x][position.y] - 1) {
        position = new_position;
        path.addNodeAtEnd(position);
      }
    }
  }
  return MazeSolverError::Ok;
}

/**
 * Template deduction guide.
 */
template <uint16_t SizeX, uint16_t SizeY>
MazeSolver(const MazeMap<SizeX, SizeY>& map) -> MazeSolver<SizeX, SizeY>;

}  // namespace MM

#endif  // MM_MAZESOLVER_H
